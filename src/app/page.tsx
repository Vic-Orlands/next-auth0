"use client"

import { useUser } from '@auth0/nextjs-auth0/client';

const Home = () => {
  const { user, isLoading, error } = useUser();

  if (isLoading) <div className='text-center mt-10'>Loading...</div>
  if (error) <div className='text-center mt-10'>{error.message}</div>

  return (
    <div className="my-5 text-center">
      {
        !user ?
          <>
            <h1 className="mb-4">
              Welcome to Next.js and Auth0 Tutorial
            </h1>

            <p className="lead">
              Log in to access your profile information (protected route)
            </p>
          </>
          :
          <>
            <h1 className="mb-4">
              Welcome {user.name}
            </h1>

            <p className="lead">
              This is a sample application that demonstrates an authentication flow for a Regular Web App, using{' '} <a href="https://auth0.com">Auth0</a> and <a href="https://nextjs.org">Next.js</a>
            </p>
          </>
      }
    </div>
  );
};

export default Home;