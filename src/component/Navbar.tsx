"use client";

import Link from "next/link";
import { useUser } from "@auth0/nextjs-auth0/client";

const Navbar = () => {
  const { user } = useUser();

  return (
    <nav className="bg-cyan-900 p-4">
      <div className="container mx-auto flex justify-between items-center">
        <Link href="/" className="text-white text-lg font-bold">
          Auth0 Tutorial
        </Link>
        <div className="flex items-center justify-between space-x-4">
          {user ? (
            <>
              <Link
                href="/profile"
                className="text-white text-lg font-medium mr-5"
              >
                Profile
              </Link>
              <a
                href="/api/auth/logout"
                className="text-white text-lg font-medium"
              >
                Logout
              </a>
            </>
          ) : (
            <a
              href="/api/auth/login"
              className="text-white text-lg font-medium"
            >
              Login
            </a>
          )}
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
